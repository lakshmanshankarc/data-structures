
class DynamicArray:
    def __init__(self,n):
        self.size=0#Orginal length or size of the array
        self.capacity=1 #Capacity is increased to 2*capacity when self.size == self.capacity
        self.arr=self._make_array(self.capacity) #Orginal array
    
    # return length of the array
    def __len__(self):
        return self.size

    def __getitem__(self,i):
        """Used for Indexing puropse eg DynamicArray[i]
           return DynamicArray[i]
        """
        if i<self.size:
            return self.arr[i]
        raise IndexError("Index is greater than the length of the Array")


    def append(self,val):
        """This method needs no explanation 
            Generally add a element at the end of the Dynamic Array
        """
        if self.size == self.capacity:
            self._resize(self.capacity*2)
            self.capacity=self.capacity*2
        self.arr[self.size]=val
        self.size+=1

    def pop(self):
        """pop method is used to remove the last element of the dynamic array 
            and simply return the element
        """
        tmp=self.arr[self.size-1]
        self.arr[self.size-1]=None
        self.size-=1
        return tmp

    def _resize(self,new_cap):
        """
            This function will generate a new array with 2*len(array) capacity
            and copy the contents of the old array to new array the make the self.array=temp
        """
        temp=self._make_array(new_cap)
        for i in range(0,self.size):
            temp[i]=self.arr[i]
        self.arr=temp
        return self.arr
    
    def _make_array(self,size):
        """Create a new Empty array of size N"""
        return [None] * size
#driver code
dd=DynamicArray(2)
dd.append(1)
dd.append(21)
dd.append(21)
dd.append(32)
dd.append(43)
print(dd.pop())
print(dd.arr)
print(len(dd))
print(dd[3])


#Date:06:10:2022 time Evening 5:30
#Lakshmanshankar.c
# DynamicArray is used to overcome the disadvantaged of traditional array
# It will dynamically increase or decrease the capacity
# It will create 2*len of the array to minimize the number of insertions needs to perform on the DynamicArray

