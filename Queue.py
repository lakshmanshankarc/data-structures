
class Queue:
    """In this Progrma I ve Implement the Queue is a simple list (Dynamic Array) in python
        Just added the name of the methods enqueue and dequeue operations
    """
    def __init__(self):
        self.queue=[]#Intialize a empty queue

    def enqueue(self,val):
        """Enqueue method will add a element at the end of the queue
            Time complexity O(1)
        """
        self.queue.append(val)
  
    def dequeue(self):
        """Dequeue operation is similar to pop Operation in stack but the difference is It will return the first element of the Queue"""
        return self.queue.pop(0)

    def __len__(self):
        """Returns the length of the queue"""
        return len(self.queue)

Q=Queue()
Q.enqueue(1)
Q.enqueue(1)
Q.enqueue(1)
Q.dequeue()

print(Q.queue,len(Q))
#Date 06:10:2022 time:Night 09:00 PM
#Lakshmanshankar C
#Queue is a linear data structure which insertion and 
# deletion are done at the different end of the queue
#Queue's are used in multiple scheduling like process scheduling and page scheduling

   
