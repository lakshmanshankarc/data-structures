
from calendar import c


class Node:
    def __init__(self,data):
        self.data=data
        self.next=None


class LinkedList:
    def __init__(self):
        self.head=None

    def printNodes(self):
        se=self.head
        while se:
            print(se.data)
            se=se.next
    
a=Node(1)
b=Node(2)
c=Node(3)
d=Node(4)

ll=LinkedList()
ll.head=a
a.next=b
b.next=c
c.next=d

ll.printNodes()