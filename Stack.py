class Stack:
    def __init__(self,size):
        self.stack=[None]*size #Generate new Empty array of given size
        self.top=-1 #Intialize the top pointer to -1
        self.size=size #Intialize the size to the given size of the stack
    def __len__(self):
        """length index start from 1 so add One to the top"""
        return self.top+1
    
    def push(self,val):
        """push Method will add new elements to the stack"""
        if self.top < self.size-1:
            self.top+=1
            self.stack[self.top]=val
        else:
            print("Stack Overflow")

    
    def pop(self):
        """Removes the last element from the stack
            Return the last element of the stack
        """
        if self.top<0:
            print("Stack Underflow")
        else:
            tmp=self.stack[self.top]
            self.stack[self.top]=None
            self.top-=1
            return tmp

    def __getitem__(self,i):
        """Used for Indexing puropse eg Stack[i]
           return Stack[i]
        """
        if i<self.top:
            return self.stack[i]
        raise IndexError("Index is greater than the length of the stack")

    def peek(self):
        """Peek method will return the top element from the stack"""
        return self.stack[self.top]

    def contains(self,val):
        """return True if the search value is present in the stack"""
        state=False
        for i in range(0,self.top+1):#for loop run one time less than the given value so increment the top by 1
            print(i,self.stack[i])
            if val==self.stack[i]:
                state=True
        return state





s=Stack(4)
s.push(1)
s.push(2)
s.push(3)
s.push(4)
print(s.stack)
print(len(s))
print(s.stack[len(s)-1])
print(s.pop())
print(len(s))
print(s.peek())
print(s.contains(4))
print(s.stack)

#Date 06:10:2022 time:Evening 06:15 PM
#Lakshmanshankar C
#stack is a linear type of data structure when the insertion and deletion are restricted to the one end of the stack
#Stack is used in many function calls 
#Advantages o(1) for insertion and deletion and efficient function management